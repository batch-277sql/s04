-- [Section] Advance selects


-- exclude records or NOT operator
SELECT * FROM songs WHERE id != 3;


-- GREATER THAN or EQUAL

SELECT * FROM songs where id > 3;
SELECT * FROM songs where id >= 3;



-- LESS THAN or Equal


SELECT * FROM songs WHERE id < 11;
SELECT * FROM songs WHERE id <= 11;


-- IN operator

SELECT * FROM songs WHERE id IN (1, 3, 11);


SELECT * FROM songs WHERE genre IN ("POP", "KPOP");


-- PARTIAL Macthes.

SELECT * FROM songs WHERE song_name LIKE "%A";


SELECT * FROM songs WHERE song_name LIKE "A%";

SELECT * FROM songs WHERE song_name LIKE "%A%";


SELECT * FROM songs WHERE song_name LIKE "%A%y%";


--SORTING recrods
SELECT * FROM songs ORDER BY song_name;
SELECT * FROM songs ORDER BY song_name DESC;


-- DISTINCT

SELECT DISTINCT genre FROM songs;



-- COUNT

SELECT COUNT(genre) FROM songs WHERE genre = "Pop";


-- [INNER JOIN]
SELECT * FROM albums JOIN songs ON albums.id = songs.album_id;

SELECT albums.album_title, songs.song_name FROM albums JOIN songs ON albums.id = songs.album_id;


-- LEFT JOIN
SELECT * FROM albums LEFT JOIN ON albums.id = songs.album_id;


-- Joining multiple tables
SELECT * FROM artists 
	JOIN albums ON artists.id = albums.artist_id
	JOIN songs ON albums.id = songs.album_id;

-- RIGHT JOIN
SELECT * FROM albums RIGHT JOIN ON albums.id = songs.album_id;



-- OUTER JOIN

SELECT * FROM albums LEFT OUTER JOIN songs ON albums.id = songs.album_id
	UNION
	SELECT * FROM albums RIGHT OUTER JOIN songs ON albums.id = songs.albums_id;




